# Register a New Amazon EC2 Container Service Task Definition
An example script and configuration for registering a new Task Definition for an existing [Amazon EC2 Container Service](https://aws.amazon.com/ecs/) (Amazon ECS) service and cluster with Bitbucket Pipelines.  This example assumes a Docker image is being built somewhere outside of this workflow, for example with [Docker Hub](https://hub.docker.com/).  The example also assumes git tags are being used to version the Docker image.  A sample Dockerfile is included to use as a demo for trying out the included code and configuration.

## How To Use It
* If you do not already have an Amazon ECS cluster and service setup, follow our [ECS Getting Started](https://us-west-2.console.aws.amazon.com/ecs/home?region=us-west-2#/firstRun) guide to create them.
    * You can uncheck the "Store container images securely with Amazon ECR" box during ECS setup and proceed with only "Deploy a sample application onto an Amazon ECS Cluster" checked.
    * You can accept all defaults at each step for the purposes of this demo.
* Setup automatic builds with Bitbucket using [Docker Hub](https://docs.docker.com/docker-hub/bitbucket/) for your repository.
    * In the Build Settings in Docker Hub, create a Build Rule that creates a Docker Image when a git tag is pushed to the repository.
        * Type:  Tag
        * Name:  leave this field blank so it targets all git tags
        * Dockerfile location:  /
        * Docker Tag Name:  leave this field blank so it uses the same name as the git tag
* Add the required Environment Variables below.
* Copy `ecs_deploy.py` to your project.
    * When using this script, you must pass three arguments in order:
      * The location of a [Container Definition JSON template](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definitions) relative to the repository.
      * The name of the Docker image to be used in the Task Definition.
      * The number of Tasks that should be running.
* Copy `bitbucket-pipelines.yml` to your project.
    * Or use your own, just be sure to include all steps in the sample yml file.
* If you followed the Getting Started guide above and are using the ECS sample application, copy the `Dockerfile` and `index.html` files to your project.

## Example Workflow
1.  Create a new branch, modify the application or Dockerfile, commit your changes, and push to your Bitbucket repository.
    * If you're using the included `Dockerfile` and `index.html` files in this repository, you can modify `index.html` to follow along.
2.  Create a Pull Request for the branch.  The instructions to do this are displayed when you push in Step 1.
3.  Normally more commits will happen in this step and you'll collaborate with your team.  For the purposes of the demo we'll pretend there is no more work to be done.  [Create a git tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging) to version your Docker Image.  
    * For the demo:  `git tag 0.1.0`
4.  Push the git tag to your repository with `git push --tags`.  The git tag being pushed into the repository triggers a Docker Hub build of the Docker image.  Documentation for setting this up can be found [here](https://docs.docker.com/docker-hub/bitbucket/).  This creates a new Docker image tagged with the version number that matches the git tag created in the step above.
    * For the demo the Docker image tag will be 0.1.0.
5.  Merge the Pull Request into the 'master' branch.  This will trigger Bitbucket Pipelines.
6.  The bitbucket-pipelines.yml includes a step for the 'master' branch that runs the script found this repository with the arguments described in the above 'How To Use It' section.  This creates a new Task Definition in Amazon ECS and registers it with an existing Service (i.e. updates the Docker image running on the cluster).
    * For the demo you do not need to modify the `bitbucket-pipelines.yml` file.
7.  The new ECS Task Definition should now be deployed to your ECS service.  If the container is still being built, the ECS service scheduler will automatically retry to place your container as soon as the Docker Image is available from Docker Hub.

## Required Environment Variables
  * `AWS_SECRET_ACCESS_KEY`:  Secret key for a user with the required permissions.
  * `AWS_ACCESS_KEY_ID`:  Access key for a user with the required permissions.
  * `AWS_DEFAULT_REGION`:  Region where the target AWS Lambda function is.
  * `ECS_SERVICE_NAME`:  Name of the ECS Service to update.
  * `ECS_CLUSTER_NAME`:  Name of the ECS Cluster the service should run on.
  * `ECS_TASK_FAMILY_NAME`:  Family name for the Task Definition.
  * `DOCKER_IMAGE`:  Location of the Docker Image to be run.  The tag/version is passed in bitbucket-pipelines.yml.

## Required Permissions in AWS
It is recommended you [create](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) a separate user account used for this deploy process.  This user should be associated with a group that has the `AmazonEC2ContainerServiceFullAccess` [AWS managed policy](http://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html) attached for the required permissions to execute.

Note that the above permissions are more than what is required in a real scenario. For any real use, you should limit the access to just the AWS resources in your context.

# License
Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Note: Other license terms may apply to certain, identified software files contained within or distributed with the accompanying software if such terms are included in the directory containing the accompanying software. Such other license terms will then apply in lieu of the terms of the software license above.
